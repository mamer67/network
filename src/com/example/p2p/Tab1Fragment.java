package com.example.p2p;

import java.net.InetAddress;
import java.util.ArrayList;

import server.Network;
import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.ActionListener;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.net.wifi.p2p.WifiP2pManager.ConnectionInfoListener;
import android.net.wifi.p2p.WifiP2pManager.PeerListListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Tab1Fragment extends Fragment implements ConnectionInfoListener, PeerListListener {
	
	private final IntentFilter intentFilter = new IntentFilter("com.example.p2p.broadcastIntent");

	private Channel wifiChannel;
	private WiFiDirectBroadcastReceiver receiver;
	private WifiP2pManager wifiManager;
	private ArrayList<WifiP2pDevice> peers = new ArrayList<WifiP2pDevice>();
	private ArrayAdapter<String> peersAdapter;
	private ArrayList<String> peersArray = new ArrayList<String>();

	
	private boolean isConnected;
	private TextView newsFeed;

	/* used to override view in fragment */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (container == null) {
			// We have different layouts, and in one of them this
			// fragment's containing frame doesn't exist. The fragment
			// may still be created from its saved state, but there is
			// no reason to try to create its view hierarchy because it
			// won't be displayed. Note this is not needed -- we could
			// just run the code below, where we would create and return
			// the view hierarchy; it would just never be used.
			return null;
		}

		// Indicates a change in the Wi-Fi P2P status.
		intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);

		// Indicates a change in the list of available peers.
		intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);

		// Indicates the state of Wi-Fi P2P connectivity has changed.
		intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);

		// Indicates this device's details have changed.
		intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

		// set up the wifiManager objects
		wifiManager = (WifiP2pManager) getActivity().getSystemService(Context.WIFI_P2P_SERVICE);
		wifiChannel = wifiManager.initialize(getActivity(), getActivity().getMainLooper(), null);
		receiver = new WiFiDirectBroadcastReceiver(wifiManager, wifiChannel, this);

		// discover surrounding peers
		wifiManager.discoverPeers(wifiChannel, new WifiP2pManager.ActionListener() {

			public void onSuccess() {
				toast("Success on discover peers");
			}

			public void onFailure(int arg0) {
				toast("Fail on discover peers");
			}
		});

		/* test connection */
		ConnectivityManager manager = (ConnectivityManager) getActivity().getSystemService(
				Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = manager.getActiveNetworkInfo();
		if (info == null) {
			// no network
			isConnected = false;
		} else {
			// network found
			isConnected = true;
		}

		/* begin setting the view */
		View fragmentView = (LinearLayout) inflater.inflate(R.layout.tab_frag1_layout, container,
				false);

		newsFeed = (TextView) fragmentView.findViewById(R.id.newsFeed);
		ListView peerslist = (ListView) fragmentView.findViewById(R.id.peersListView);
		peersAdapter = new ArrayAdapter<String>(getActivity(), R.layout.peeritem, peersArray);
		peerslist.setAdapter(peersAdapter);

		Button disconnect = (Button) fragmentView.findViewById(R.id.disconnectButton);
		disconnect.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				Network.stop();
				wifiManager.removeGroup(wifiChannel, new ActionListener() {

					@Override
					public void onFailure(int reasonCode) {
						Log.d(MainActivity.TAG, "Disconnect failed. Reason :" + reasonCode);

					}

					@Override
					public void onSuccess() {
						Toast.makeText(Tab1Fragment.this.getActivity(), "Group disconnected.",
								Toast.LENGTH_SHORT).show();
					}

				});
			}
		});

		Button search = (Button) fragmentView.findViewById(R.id.searchButton);
		search.setOnClickListener(new View.OnClickListener() {

			public void onClick(View view) {
				// discover pears
				wifiManager.discoverPeers(wifiChannel, new WifiP2pManager.ActionListener() {

					public void onSuccess() {
						toast("Success on discover peers");
					}

					public void onFailure(int fail) {
						toast("Fail on discover peers");
					}
				});
			}
		});

		// add listener to the list
		peerslist.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
				// Picking the first device found on the network.
				WifiP2pDevice device = peers.get(position);
				WifiP2pConfig config = new WifiP2pConfig();
				config.deviceAddress = device.deviceAddress;
				config.wps.setup = WpsInfo.PBC;

				wifiManager.connect(wifiChannel, config, new ActionListener() {
					@Override
					public void onSuccess() {
						// WiFiDirectBroadcastReceiver will notify us. Ignore for now.
					}

					@Override
					public void onFailure(int reason) {
						toast("Connect failed. Retry.");
					}
				});
			}
		});

		return fragmentView;
	}

	@Override
	public void onResume() {
		super.onResume();
		getActivity().registerReceiver(receiver, intentFilter);
	}

	@Override
	public void onPause() {
		super.onPause();
		getActivity().unregisterReceiver(receiver);
	}

	public void onPeersAvailable(WifiP2pDeviceList peerList) {

		// Out with the old, in with the new.
		peers.clear();
		peers.addAll(peerList.getDeviceList());

		// updates list
		peersArray.clear();
		for (int i = 0; i < peers.size(); i++) {
			peersArray.add(peers.get(i).deviceName + " " + peers.get(i).deviceAddress);
		}

		peersAdapter.notifyDataSetChanged();
	}

	public void onConnectionInfoAvailable(WifiP2pInfo info) {
		// InetAddress from WifiP2pInfo struct.
		InetAddress groupOwnerAddress = info.groupOwnerAddress;

		// After the group negotiation, we can determine the group owner.
		if (info.groupFormed && info.isGroupOwner) {
			// Do whatever tasks are specific to the group owner.
			// One common case is creating a server thread and accepting
			// incoming connections.
			newsFeed.setText("I am group owner server");
			Log.v(MainActivity.TAG, "I am group owner "
					+ new String(groupOwnerAddress.getAddress()));

			// create network instance
			Network.create(true, Environment.getExternalStorageDirectory() + "/Sharable",
					info.groupOwnerAddress, MainActivity.PORT, isConnected);
		} else if (info.groupFormed) {
			// The other device acts as the client. In this case,
			// you'll want to create a client thread that connects to the group
			// owner.
			newsFeed.setText("I am group client " + groupOwnerAddress.toString());
			Log.v(MainActivity.TAG,
					"I am group client " + new String(groupOwnerAddress.getAddress()));

			// create network instance
			Network.create(false, Environment.getExternalStorageDirectory() + "/Sharable",
					info.groupOwnerAddress, MainActivity.PORT, isConnected);
		}

		if (info.groupFormed) {
			new StartNetworkAsyncTask().execute(new Void[] {});
		}
	}

	public class StartNetworkAsyncTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {

			Network network = Network.getInstance();

			while (network == null) {
				network = Network.getInstance();
			}

			// start network
			try {
				network.start();
			} catch (Exception e) {
				e.printStackTrace();
			}

			return null;
		}
	}

	/* show toast */
	public void toast(String str) {
		Toast.makeText(Tab1Fragment.this.getActivity(), str, Toast.LENGTH_SHORT).show();
	}
}