package com.example.p2p;

import server.Network;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.widget.Toast;

public class WiFiDirectBroadcastReceiver extends BroadcastReceiver {
	private WifiP2pManager wifiManager;
	private Channel wifiChannel;
	private Tab1Fragment fragment;

	public WiFiDirectBroadcastReceiver(WifiP2pManager manager, Channel channel,
			Tab1Fragment activity) {
		super();
		this.wifiManager = manager;
		this.wifiChannel = channel;
		this.fragment = activity;
	}

	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();

		if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
			// Determine if wifi P2P mode is enabled or not, alert the Activity.
			int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);

			if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
				toast("Wifi Enable");
			} else {
				toast("Wifi Disable");
			}
		} else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {
			// The peer list has changed! We should probably do something about that.
			if (wifiManager != null) {
				wifiManager.requestPeers(wifiChannel, fragment);
			}
		} else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {
			NetworkInfo nwInfo = (NetworkInfo) intent
					.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);

			if (nwInfo.isConnected()) {
				wifiManager.requestConnectionInfo(wifiChannel, fragment);
			} else {
				Network.stop();
			}
		} else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {
		}
	}

	/* show toast */
	private void toast(String str) {
		Toast.makeText(fragment.getActivity(), str, Toast.LENGTH_SHORT).show();
	}
}
