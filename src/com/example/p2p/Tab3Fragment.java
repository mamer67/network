package com.example.p2p;

import server.Network;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

public class Tab3Fragment extends Fragment {

	private WebView mWebView;
	private EditText textInput;

	private Network network;
	private ProgressDialog progress;

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (container == null) {
			// We have different layouts, and in one of them this
			// fragment's containing frame doesn't exist.  The fragment
			// may still be created from its saved state, but there is
			// no reason to try to create its view hierarchy because it
			// won't be displayed.  Note this is not needed -- we could
			// just run the code below, where we would create and return
			// the view hierarchy; it would just never be used.
			return null;
		}

		// set up view
		View fragmentView = null;

		network = Network.getInstance();
		if (network == null || network.isRunning() == false) {
			fragmentView = (RelativeLayout) inflater.inflate(R.layout.no_connection_layout,
					container, false);
		} else {
			fragmentView = (RelativeLayout) inflater.inflate(R.layout.tab_frag3_layout, container,
					false);

			mWebView = (WebView) fragmentView.findViewById(R.id.webView);
			textInput = (EditText) fragmentView.findViewById(R.id.urlInput);

			Button button = (Button) fragmentView.findViewById(R.id.goButton);

			button.setOnClickListener(new View.OnClickListener() {

				public void onClick(View arg0) {
					String host = textInput.getText().toString();
					String url = "";
					if (host.startsWith("http://") == false)
						url = "http://" + host;
					else
						url = host;

					progress.show();
					// url
					new URLAsyncTask().execute(new String[] { url });
				}
			});

			mWebView.setWebViewClient(new WebViewClient() {

				public boolean shouldOverrideUrlLoading(WebView view, String url) {

					//mWebView.loadUrl(url);
					return false;
				}

				/*@Override
				public void onLoadResource(WebView view, String url) {
					textInput.setText(url);
					
					// url
					new URLAsyncTask().execute(new String[] { url });
				}*/
			});

			// create progress
			progress = new ProgressDialog(getActivity());
			progress.setMessage("Fetching html :)");
			progress.setIndeterminate(true);
			//progress.setCancelable(false);
		}

		return fragmentView;
	}

	public class URLAsyncTask extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {

			try {
				network.processFetchFilename(params[0], true);
			} catch (Exception e) {

			}

			return params[0];
		}

		@Override
		protected void onPostExecute(String url) {
			progress.dismiss();
			mWebView.loadUrl("file://" + network.getDirecotyPath() + "/"
					+ Network.getNameFromURL(url));
		}
	}

}