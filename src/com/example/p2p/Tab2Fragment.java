package com.example.p2p;

import java.io.File;
import java.util.ArrayList;

import server.Network;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;


public class Tab2Fragment extends Fragment {

	private Network network;

	private ArrayList<String> files = new ArrayList<String>();
	private ArrayAdapter<String> filesAdapter;

	private ProgressDialog progress;

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (container == null) {
			// We have different layouts, and in one of them this
			// fragment's containing frame doesn't exist.  The fragment
			// may still be created from its saved state, but there is
			// no reason to try to create its view hierarchy because it
			// won't be displayed.  Note this is not needed -- we could
			// just run the code below, where we would create and return
			// the view hierarchy; it would just never be used.
			return null;
		}

		// set up view
		View fragmentView = null;

		network = Network.getInstance();
		if (network == null || network.isRunning() == false) {
			fragmentView = (RelativeLayout) inflater.inflate(R.layout.no_connection_layout,
					container, false);
		} else {
			fragmentView = (LinearLayout) inflater.inflate(R.layout.tab_frag2_layout, container,
					false);

			// create progress
			progress = new ProgressDialog(getActivity());
			progress.setMessage("Fetching File :)");
			progress.setIndeterminate(true);
			//progress.setCancelable(false);

			// get list view
			ListView fileList = (ListView) fragmentView.findViewById(R.id.filelist);
			files.clear();
			files.addAll(Network.index);
			filesAdapter = new ArrayAdapter<String>(getActivity(), R.layout.peeritem, files);
			fileList.setAdapter(filesAdapter);

			fileList.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> adapter, View vw, final int position, long id) {
					progress.show();
					new FileServerAsyncTask().execute(new Integer[] { position });
				}
			});
		}
		return fragmentView;
	}

	public class FileServerAsyncTask extends AsyncTask<Integer, Void, String> {

		/**
		 * @param context
		 * @param statusText
		 */

		@Override
		protected String doInBackground(Integer... params) {
			String filename = files.get(params[0]);

			try {
				network.processFetchFilename(filename, false);
			} catch (Exception e) {

			}

			return filename;
		}

		@Override
		protected void onPostExecute(String filePath) {
			progress.dismiss();

			File file = new File(network.getDirecotyPath() + "/" + filePath);

			System.out.println(file.getAbsolutePath());
			// open file
			Uri uri = Uri.fromFile(file);
			Intent intent = new Intent(Intent.ACTION_VIEW);
			Global.setMIMI(intent, uri, filePath);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
		}
	}
}