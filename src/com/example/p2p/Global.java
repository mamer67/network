package com.example.p2p;

import android.content.Intent;
import android.net.Uri;

public class Global {

	public static void setMIMI(Intent intent, Uri uri, String filePath) {
		if (filePath.contains(".doc") || filePath.contains(".docx")) {
			// Word document
			intent.setDataAndType(uri, "application/msword");
		} else if (filePath.contains(".pdf")) {
			// PDF file
			intent.setDataAndType(uri, "application/pdf");
		} else if (filePath.contains(".ppt") || filePath.contains(".pptx")) {
			// Powerpoint file
			intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
		} else if (filePath.contains(".xls") || filePath.contains(".xlsx")) {
			// Excel file
			intent.setDataAndType(uri, "application/vnd.ms-excel");
		} else if (filePath.contains(".zip") || filePath.contains(".rar")) {
			// WAV audio file
			intent.setDataAndType(uri, "application/x-wav");
		} else if (filePath.contains(".rtf")) {
			// RTF file
			intent.setDataAndType(uri, "application/rtf");
		} else if (filePath.contains(".wav") || filePath.contains(".mp3")
				|| filePath.contains(".ogg")) {
			// WAV audio file
			intent.setDataAndType(uri, "audio/x-wav");
		} else if (filePath.contains(".gif")) {
			// GIF file
			intent.setDataAndType(uri, "image/gif");
		} else if (filePath.contains(".jpg") || filePath.contains(".jpeg")
				|| filePath.contains(".png")) {
			// JPG file
			intent.setDataAndType(uri, "image/jpeg");
		} else if (filePath.contains(".txt")) {
			// Text file
			intent.setDataAndType(uri, "text/plain");
		} else if (filePath.contains(".3gp") || filePath.contains(".mpg")
				|| filePath.contains(".mpeg") || filePath.contains(".mpe")
				|| filePath.contains(".mp4") || filePath.contains(".avi")) {
			// Video files
			intent.setDataAndType(uri, "video/*");
		} else if (filePath.contains(".html") || filePath.contains(".xml")) {
			intent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
			intent.setData(uri);
		} else {
			// if you want you can also define the intent type for any other
			// file

			// additionally use else clause below, to manage other unknown
			// extensions
			// in this case, Android will show all applications installed on
			// the
			// device
			// so you can choose which application to use

			intent.setDataAndType(uri, "*/*");
		}
	}
}
