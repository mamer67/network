package com.example.p2p;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class Tab extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (container == null) {
			// We have different layouts, and in one of them this
			// fragment's containing frame doesn't exist. The fragment
			// may still be created from its saved state, but there is
			// no reason to try to create its view hierarchy because it
			// won't be displayed. Note this is not needed -- we could
			// just run the code below, where we would create and return
			// the view hierarchy; it would just never be used.
			return null;
		}
		View v = inflater.inflate(R.layout.activity_main, container, false);
		return v;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		Log.v("json", "it jumps here too");
	}

	// to switch to this fragment
	/*FragmentManager fragmentManager2 = Tab1Fragment.this.getFragmentManager();

	FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();

	Tab fragment2 = new Tab();

	fragmentTransaction2.addToBackStack("abc");

	fragmentTransaction2.hide(Tab1Fragment.this);

	fragmentTransaction2.add(android.R.id.content, fragment2);

	fragmentTransaction2.commit();*/
}
