package server;

import java.io.Serializable;
import java.util.ArrayList;

public class PNode implements Serializable {

	private static final long serialVersionUID = 1L;

	private boolean connected;
	private ArrayList<String> files;

	public PNode(boolean hasC, ArrayList<String> fil) {
		connected = hasC;
		files = fil;
	}

	public boolean isConnected() {
		return connected;
	}

	public ArrayList<String> getFiles() {
		return files;
	}
}
