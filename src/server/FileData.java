package server;

import java.io.Serializable;

public class FileData implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private byte[] data;
	private int length;
	private String filename;

	public FileData(byte[] dta, int l, String fn) {
		data = dta;
		length = l;
		filename = fn;
	}

	public byte[] getData() {
		return data;
	}

	public int getLength() {
		return length;
	}

	public String getFilename() {
		return filename;
	}

	public boolean isFinalPacket() {
		return length <= 0;
	}

}
