package server;

import java.io.Serializable;

public class Packet implements Serializable {

	private static final long serialVersionUID = 1L;

	// messages types
	public static final int SETUP = 0x1;
	public static final int UPDATE = 0x2;
	public static final int FetchFileName = 0x3;
	public static final int FetchFileNameResponse = 0x4;
	public static final int FetchURL = 0x5;
	public static final int FetchURLResponse = 0x6;
	public static final int DATA = 0x7;

	private int type;
	private Object obj;

	public Packet(int typ, Object ob) {
		type = typ;
		obj = ob;
	}

	public int getType() {
		return type;
	}

	public Object getObject() {
		return obj;
	}
}
