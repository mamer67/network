package server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.concurrent.ConcurrentHashMap;

/*
 * Server class assumes no channel loss and reliable data streams
 * as its build on TCP which handles all this
 * */
public class Network {
	public static ArrayList<String> index = new ArrayList<String>();

	// Coding here
	public static final int MAXBUFFER = 1024;
	public static final int REMOTE = -2;
	public static final int LOCAL = -1;
	private ArrayList<Socket> socketsToClients = new ArrayList<Socket>();
	private ArrayList<Socket> socketDataToClients = new ArrayList<Socket>();
	private Socket socketToServer;
	private Socket dataSocketToServer;
	private ConcurrentHashMap<String, Integer> files = new ConcurrentHashMap<String, Integer>();
	private ArrayList<Integer> online = new ArrayList<Integer>();

	private boolean isServer;
	private String directoryPath;
	private InetAddress host;
	private int port;

	private static boolean started = false;
	private static boolean isConnected = false;
	private static boolean run = true;
	private static Network currentInstance;

	private Network(boolean server, String path, InetAddress host, int port) {
		this.isServer = server;
		this.directoryPath = path;
		this.host = host;
		this.port = port;
	}

	public synchronized static void create(boolean server, String path, InetAddress host, int port,
			boolean connected) {
		if (started == false) {
			run = true;
			started = true;
			isConnected = connected;
			currentInstance = new Network(server, path, host, port);

			System.out.println("******************");
		}
	}

	public static Network getInstance() {
		return currentInstance;
	}

	public synchronized static void stop() {
		run = false;
		started = false;
		currentInstance = null;

		System.out.println("------------------");
	}

	public boolean isRunning() {
		return run;
	}

	// list all files inside a directory
	private ArrayList<String> listAllFilesInDirectory(String directoryName) {
		File directory = new File(directoryName);
		ArrayList<String> names = new ArrayList<String>();

		// get all the files from a directory
		File[] fList = directory.listFiles();
		for (File file : fList) {
			if (file.isFile()) {
				names.add(file.getName());
			} else if (file.isDirectory()) {
				listAllFilesInDirectory(file.getAbsolutePath());
			}
		}

		return names;
	}

	// This method goes for the button start server, start client
	public void start() throws IOException, ClassNotFoundException {
		if (isServer) {
			// scan for files in directory
			ArrayList<String> fles = listAllFilesInDirectory(directoryPath);

			// put all the files in the map
			addFiles(fles, LOCAL);

			// thread to open server
			new Thread(new Runnable() {

				@Override
				public void run() {
					ServerSocket welcomeSocket = null;

					// open server connection
					do {
						try {
							welcomeSocket = new ServerSocket(port);
							printLog("Server started");
						} catch (Exception e1) {
							//printLog("Error starting server connection");
							//e1.printStackTrace();
						}
					} while (welcomeSocket == null);

					// accept connections for ever
					while (run) {
						try {
							Socket socketToClient = welcomeSocket.accept();

							// start running accepting connections in background
							Thread run_accept = new Thread(new Accept(socketToClient));
							run_accept.start();
						} catch (IOException e1) {
							printLog("Error in accepting connection");
							e1.printStackTrace();
						}
					} // end while
				}
			}).start();

		} else {
			new Thread(new Runnable() {

				@Override
				public void run() {
					do {
						try {
							socketToServer = new Socket(host, port);
							dataSocketToServer = new Socket(host, port);
						} catch (Exception e) {
							// printLog("Error in connecting to server, will try again..");
						}
						try {
							Thread.sleep(20);
						} catch (InterruptedException e) {
							printLog("Error in sleeping");
							e.printStackTrace();
						}
					} while (socketToServer == null && dataSocketToServer == null);

					try {
						// setup data socket
						setupDataLine();

						// setup client socket
						processSetup();
					} catch (Exception e) {
						printLog("Error in processSetup, setupDataLine");
						e.printStackTrace();
					}

				}
			}).start();
		}
	}

	private void setupDataLine() throws IOException {
		// create a message
		Packet msg = new Packet(Packet.DATA, null);

		// send object to master
		writeMessage(dataSocketToServer, msg);

		// create thread to listen to server if needs data from me
		new Thread(new Runnable() {

			@SuppressWarnings("unchecked")
			@Override
			public void run() {
				while (run) {
					// wait for message
					try {
						Packet msg = readMessage(dataSocketToServer);

						if (msg != null) {
							int type = msg.getType();
							if (type == Packet.FetchFileName || type == Packet.FetchURL) {
								String filename = (String) msg.getObject();
								int response;

								if (type == Packet.FetchURL) {
									filename = fetchURL(filename);
									response = Packet.FetchURLResponse;
								} else {
									response = Packet.FetchFileNameResponse;
								}

								FileInputStream input = new FileInputStream(new File(directoryPath
										+ "/" + filename));

								// do the read and send back to server
								int length = 0;
								do {
									// read
									byte[] buffer = new byte[1024];
									length = input.read(buffer);
									writeMessage(dataSocketToServer, new Packet(response,
											new FileData(buffer, length, filename)));
								} while (length > 0);

								//close 
								input.close();
							} else if (type == Packet.UPDATE) {
								if ((msg.getObject() instanceof ArrayList<?>) == true) {
									ArrayList<String> newFiles = (ArrayList<String>) msg
											.getObject();

									// deal with files
									addFiles(newFiles, REMOTE);

									addTheFilesIntoIndex(newFiles);
									printLog(files.toString());
								} else {
									printLog("unknow object inside update message");
								}
							} else {
								// 
								System.out
										.println("Unsupported type of message in thread at client");
							}
						} else {
							printLog("unknown null message in thread at client");
						}

					} catch (Exception e) {
						printLog("Error in thread at client");
						e.printStackTrace();
					}
				}
			}
		}).start();
	}

	@SuppressWarnings("unchecked")
	private void processSetup() throws IOException, ClassNotFoundException {
		// scan for files in directory
		ArrayList<String> fles = listAllFilesInDirectory(directoryPath);

		// put all the files in the map
		addFiles(fles, LOCAL);

		PNode node = new PNode(isConnected, fles);

		// create a message
		Packet msg = new Packet(Packet.SETUP, node);

		// send object to master
		writeMessage(socketToServer, msg);

		// receive message from master
		Packet newMessage = readMessage(socketToServer);

		if (newMessage != null && newMessage.getType() == Packet.UPDATE
				&& (newMessage.getObject() instanceof ArrayList<?>) == true) {

			ArrayList<String> newFiles = (ArrayList<String>) newMessage.getObject();

			// deal with files
			addFiles(newFiles, REMOTE);

			addTheFilesIntoIndex(newFiles);
			printLog(files.toString());
		} else {
			// unknown state
			printLog("Error, Expecting Update message");
		}
	}

	private void addTheFilesIntoIndex(ArrayList<String> files) {
		index.clear();
		index.addAll(files);
	}

	/**
	 * get the filename from network
	 * 
	 * @param filename
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void processFetchFilename(String filename, boolean isURL) throws IOException,
			ClassNotFoundException {
		Integer where = null;
		int type = 0;
		int response = 0;

		if (isURL) {
			type = Packet.FetchURL;
			response = Packet.FetchURLResponse;
			if (isConnected)
				where = LOCAL;
			else if (!isConnected && !isServer)
				where = REMOTE;
			else if (!isConnected && isServer) {
				if (online.size() > 0)
					where = online.get(0);
			}
		} else {
			type = Packet.FetchFileName;
			response = Packet.FetchFileNameResponse;
			where = files.get(filename);
		}

		if (where != null) {
			if (where == LOCAL) {
				// just open file
				if (isURL) {
					fetchURL(filename);
				}
			} else if (where == REMOTE) {
				// read file from remote server
				// send message to remote server
				writeMessage(socketToServer, new Packet(type, new String(filename)));

				String name = filename;
				if (isURL)
					name = getNameFromURL(name);

				// open stream to save file
				FileOutputStream output = new FileOutputStream(new File(directoryPath + "/" + name));

				
				FileData fdata = null;

				do {
					Packet incoming = readMessage(socketToServer);
					if (incoming != null && incoming.getType() == response
							&& incoming.getObject() instanceof FileData == true) {
						// get file data
						fdata = (FileData) incoming.getObject();

						if (!fdata.isFinalPacket())
							output.write(fdata.getData(), 0, fdata.getLength());
					} else {
						// unknown message
						printLog("Unknown message in FetchFileNameReponse");
					}
				} while (fdata.isFinalPacket() == false);

				// close 
				output.close();
			} else if (where >= 0) {
				// read file from remote client
				// send message to remote client
				Socket src = socketDataToClients.get(where);
				writeMessage(src, new Packet(type, new String(filename)));

				
				String name = filename;
				if (isURL)
					name = getNameFromURL(name);

				// open stream to save file
				FileOutputStream output = new FileOutputStream(new File(directoryPath + "/" + name));

				
				FileData fdata = null;

				do {
					Packet incoming = readMessage(src);
					if (incoming != null && incoming.getType() == response
							&& incoming.getObject() instanceof FileData == true) {
						// get file data
						fdata = (FileData) incoming.getObject();

						if (!fdata.isFinalPacket())
							output.write(fdata.getData(), 0, fdata.getLength());
					} else {
						// unknown message
						printLog("Unknown message in FetchFileNameReponse");
					}
				} while (fdata.isFinalPacket() == false);

				// close 
				output.close();
			}
		} else {
			// not the case, we choose from list
			printLog("File not found, this shouldn't happen");
		}
	}

	private void processFetchFilenameServer(String filename, boolean isURL, int dest)
			throws IOException, ClassNotFoundException {
		Integer where = null;

		int type = 0;
		int response = 0;

		if (isURL) {
			type = Packet.FetchURL;
			response = Packet.FetchURLResponse;
			if (isConnected)
				where = LOCAL;
			else if (!isConnected && isServer) {
				if (online.size() > 0)
					where = online.get(0);
			}
		} else {
			type = Packet.FetchFileName;
			response = Packet.FetchFileNameResponse;
			where = files.get(filename);
		}

		if (where == LOCAL) {
			String name = filename;

			if (isURL) {
				name = fetchURL(filename);
			}

			Socket dst = socketsToClients.get(dest);

			// File already exist here
			FileInputStream input = new FileInputStream(new File(directoryPath + "/" + name));

			// do the read and send back to server
			int length = 0;
			do {
				// read
				byte[] buffer = new byte[1024];
				length = input.read(buffer);
				writeMessage(dst, new Packet(response, new FileData(buffer, length, name)));
			} while (length > 0);

			//close 
			input.close();
		} else if (where > LOCAL) {
			Socket src = socketDataToClients.get(where);
			Socket dst = socketsToClients.get(dest);

			// send message to remote phone
			writeMessage(src, new Packet(type, new String(filename)));

			FileData fdata = null;

			do {
				Packet incoming = readMessage(src);
				if (incoming != null && incoming.getType() == response
						&& incoming.getObject() instanceof FileData == true) {
					// get file data
					fdata = (FileData) incoming.getObject();
					writeMessage(dst, new Packet(response, fdata));
				} else {
					// unknown message
					printLog("Unknown message in FetchFileNameReponse");
				}
			} while (fdata.isFinalPacket() == false);

		} else {
			// not the case, we choose from list
			printLog("File not found, this shouldn't happen");
		}
	}

	private String fetchURL(String URL) throws IOException {
		String name = getNameFromURL(URL);

		java.net.URL oracle = new java.net.URL(URL);
		BufferedReader in = new BufferedReader(new InputStreamReader(oracle.openStream()));

		//
		String inputLine;
		FileWriter outputfile = new FileWriter(directoryPath + "/" + name);

		while ((inputLine = in.readLine()) != null) {
			outputfile.append(inputLine);
		}

		outputfile.close();
		in.close();

		return name;
	}

	public static String getNameFromURL(String URL) {
		String name = URL.replaceAll("\\W+", "");
		name = name.substring(name.length() > 255 ? name.length() - 255 : 0, name.length());

		return name;
	}

	private void addFiles(ArrayList<String> newFiles, int src) {
		// deal with new files
		for (int i = 0; i < newFiles.size(); i++) {
			if (!files.containsKey(newFiles.get(i))) {
				files.put(newFiles.get(i), src);
			}
		}
	}

	private Packet readMessage(Socket connection) throws ClassNotFoundException, IOException {
		// receive message from master
		ObjectInputStream input = new ObjectInputStream(connection.getInputStream());
		Packet newMessage = (Packet) input.readObject();

		return newMessage;
	}

	private void writeMessage(Socket connection, Packet msg) throws IOException {
		ObjectOutputStream output = new ObjectOutputStream(connection.getOutputStream());
		output.writeObject(msg);
	}

	class Accept implements Runnable {
		private Socket incomingClient;
		private int socketnum;

		public Accept(Socket socket) {
			incomingClient = socket;
		}

		@Override
		public void run() {
			while (run) {
				try {
					// read message from client
					ObjectInputStream input = new ObjectInputStream(incomingClient.getInputStream());

					// read message
					Packet msg = (Packet) input.readObject();

					if (msg != null) {
						int type = msg.getType();

						if (type == Packet.SETUP) {
							synchronized (socketsToClients) {
								socketsToClients.add(incomingClient);
								socketnum = socketsToClients.size() - 1;
							}

							// parse object
							if (msg.getObject() instanceof PNode == true) {
								PNode node = (PNode) msg.getObject();

								// add this node to online list if has connection
								if (node.isConnected()) {
									online.add(socketnum);
								}

								// deal with files
								addFiles(node.getFiles(), socketnum);

								ArrayList<String> fls = new ArrayList<String>();

								// empty files into this array
								Enumeration<String> fs = files.keys();

								while (fs.hasMoreElements())
									fls.add(fs.nextElement());

								// show new files
								addTheFilesIntoIndex(fls);
								printLog(files.toString());

								// create new Response message and send it
								writeMessage(incomingClient, new Packet(Packet.UPDATE, fls));

								// send to all other clients
								for (int i = 0; i < socketsToClients.size(); i++) {
									// create new Response message and send it
									writeMessage(socketDataToClients.get(i), new Packet(
											Packet.UPDATE, fls));
								}
							} else {
								//unknown state
								printLog("Unkonow object inside Setup message, server");
							}
						} else if (type == Packet.FetchFileName || type == Packet.FetchURL) {
							processFetchFilenameServer((String) msg.getObject(),
									type == Packet.FetchURL ? true : false, socketnum);
						} else if (type == Packet.DATA) {
							synchronized (socketDataToClients) {
								socketDataToClients.add(incomingClient);
							}
							break;
						}
					}// end of check msg
				} catch (Exception e) {
					printLog("Error in server, when accepting client");
					e.printStackTrace();
				}
			}
		}
	}

	public void printLog(String str) {
		System.out.println(str);
	}

	public String getDirecotyPath() {
		return directoryPath;
	}
}